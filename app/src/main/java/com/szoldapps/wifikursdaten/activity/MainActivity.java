package com.szoldapps.wifikursdaten.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.szoldapps.wifikursdaten.R;
import com.szoldapps.wifikursdaten.adapter.CourseAdapter;
import com.szoldapps.wifikursdaten.helper.JSONHelper;
import com.szoldapps.wifikursdaten.listener.OnLoadMoreListener;
import com.szoldapps.wifikursdaten.model.Course;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private JSONHelper mJSONHelper;
    private CourseAdapter mCourseAdapter;
    private boolean mAllCoursesLoaded = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mJSONHelper = new JSONHelper(getApplicationContext());

        RecyclerView rvCourses = (RecyclerView) findViewById(R.id.recycleView);

        if (rvCourses != null) {
            rvCourses.setLayoutManager(new LinearLayoutManager(this));

            mCourseAdapter = new CourseAdapter(rvCourses, getApplicationContext());
            rvCourses.setAdapter(mCourseAdapter);

            mCourseAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    if(!mAllCoursesLoaded) {
                        // add loading item
                        mCourseAdapter.addCourse(null);
                        // load more courses
                        asyncLoadMoreCourses();
                    }
                }
            });

            asyncLoadMoreCourses();
        }
    }

    private void asyncLoadMoreCourses() {
        new AsyncTask<Integer, Void, Void>() {
            List<Course> mAsyncCourses = new ArrayList<>();
            int mCoursesSize = 0;

            @Override
            protected Void doInBackground(Integer... params) {

                try {   // just to actually show the loading animation
                    Thread.sleep(1000);
                } catch (InterruptedException e) {  e.printStackTrace(); }

                if (params.length > 0) {
                    mCoursesSize = params[0];
                    int end = (mCoursesSize-1) + 20;
                    mAsyncCourses = mJSONHelper.getCoursesFromJson((mCoursesSize-1), end);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if ((mCoursesSize - 1) > 0) {
                    // Remove loading item (if displayed)
                    mCourseAdapter.popCourse();
                }

                // This is done to prevent unnecessary LoadMoreCourses calls
                if(mAsyncCourses.size() == 0) {
                    mAllCoursesLoaded = true;
                }

                // Load data
                mCourseAdapter.addCourses(mAsyncCourses);
            }
        }.execute(mCourseAdapter.getItemCount());
    }

}