package com.szoldapps.wifikursdaten.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.szoldapps.wifikursdaten.R;
import com.szoldapps.wifikursdaten.adapter.CourseDatesAdapter;
import com.szoldapps.wifikursdaten.model.Course;

public class CourseDetailActivity extends AppCompatActivity {

    public static final String EXTRA_COURSE = CourseDetailActivity.class.getSimpleName() + ".EXTRA_COURSE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_detail);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        TextView tvCourseId = (TextView) findViewById(R.id.tvDetailCourseId);
        TextView tvTitle = (TextView) findViewById(R.id.tvDetailTitle);
        TextView tvPlace = (TextView) findViewById(R.id.tvDetailPlace);
        RecyclerView rvCourseDates = (RecyclerView) findViewById(R.id.rvDetail);

        Course course = (Course) getIntent().getSerializableExtra(EXTRA_COURSE);
        if(course != null) {
            if (tvCourseId != null && tvTitle != null && tvPlace != null) {
                tvCourseId.setText(getString(R.string.label_id, course.getCourseId()));
                tvTitle.setText(course.getTitle());
                tvPlace.setText(getString(R.string.label_where, course.getPlace(), course.getProvince()));
            }

            CourseDatesAdapter mAdapter = new CourseDatesAdapter(course, getApplicationContext());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            if (rvCourseDates != null) {
                rvCourseDates.setLayoutManager(mLayoutManager);
                rvCourseDates.setItemAnimator(new DefaultItemAnimator());
                rvCourseDates.setAdapter(mAdapter);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // Back button (top-left)
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
