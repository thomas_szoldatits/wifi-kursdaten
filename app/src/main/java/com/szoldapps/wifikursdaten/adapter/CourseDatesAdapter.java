package com.szoldapps.wifikursdaten.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.szoldapps.wifikursdaten.R;
import com.szoldapps.wifikursdaten.model.Course;
import com.szoldapps.wifikursdaten.model.CourseDate;

import java.util.ArrayList;
import java.util.Collections;

public class CourseDatesAdapter extends RecyclerView.Adapter<CourseDatesAdapter.MyViewHolder> {

    private final Course mCourse;
    private ArrayList<CourseDate> mCourseDateList;
    private final Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView headline, id, street, startDate, endDate, duration, zip;
        public RelativeLayout rlRow;
        public MyViewHolder(View view) {
            super(view);
            rlRow = (RelativeLayout) view.findViewById(R.id.rlDateRow);
            headline = (TextView) view.findViewById(R.id.tvDateHL);
            id = (TextView) view.findViewById(R.id.tvDateId);
            street = (TextView) view.findViewById(R.id.tvDateStreet);
            startDate = (TextView) view.findViewById(R.id.tvDateStart);
            endDate = (TextView) view.findViewById(R.id.tvDateEnd);
            duration = (TextView) view.findViewById(R.id.tvDateDuration);
            zip = (TextView) view.findViewById(R.id.tvDateZip);

        }
    }


    public CourseDatesAdapter(Course course, Context context) {
        mCourse = course;
        mCourseDateList = course.getCourseDates();
        Collections.sort(mCourseDateList);
        mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_date_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CourseDate courseDate = mCourseDateList.get(position);
        holder.id.setText(mContext.getString(R.string.label_id_extended, courseDate.getCourseDateId(),
                courseDate.getCourseType(), courseDate.getOrganiser()));
        holder.street.setText(courseDate.getStreet());
        if(courseDate.getStartDate().equals(courseDate.getEndDate())) {
            holder.headline.setText(courseDate.getCombinedStartEndDateString(mContext));
            holder.startDate.setVisibility(View.GONE);
            holder.endDate.setVisibility(View.GONE);
        } else {
            holder.headline.setText(courseDate.getStartDateString(mContext));
            holder.startDate.setText(mContext.getString(R.string.label_from, courseDate.getStartDateString(mContext)));
            holder.endDate.setText(mContext.getString(R.string.label_to, courseDate.getEndDateString(mContext)));
            holder.startDate.setVisibility(View.VISIBLE);
            holder.endDate.setVisibility(View.VISIBLE);
        }
        holder.duration.setText(mContext.getString(R.string.label_duration, courseDate.getDuration()));
        holder.zip.setText(mContext.getString(R.string.label_zip, courseDate.getZip(), mCourse.getPlace()));
        holder.rlRow.setTag(courseDate.getUrl());
        holder.rlRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = (String) v.getTag();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                v.getContext().startActivity(browserIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCourseDateList.size();
    }
}