package com.szoldapps.wifikursdaten.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.szoldapps.wifikursdaten.R;
import com.szoldapps.wifikursdaten.activity.CourseDetailActivity;
import com.szoldapps.wifikursdaten.listener.OnLoadMoreListener;
import com.szoldapps.wifikursdaten.model.Course;

import java.util.ArrayList;
import java.util.List;

public class CourseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Course> mCourses = new ArrayList<>();
    private Context mContext;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private OnLoadMoreListener mOnLoadMoreListener;

    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public CourseAdapter(RecyclerView rv, Context context) {
        mContext = context;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) rv.getLayoutManager();
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return mCourses.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_course_item, parent, false);
            return new CourseViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CourseViewHolder) {
            Course course = mCourses.get(position);
            CourseViewHolder cvh = (CourseViewHolder) holder;
            cvh.tvCourseId.setText(mContext.getString(R.string.label_id, course.getCourseId()));
            cvh.tvTitle.setText(course.getTitle());
            cvh.tvPlace.setText(mContext.getString(R.string.label_where, course.getPlace(), course.getProvince()));
            cvh.rlCard.setTag(course);
            cvh.rlCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Course c = (Course) v.getTag();

                    Intent intent = new Intent(mContext, CourseDetailActivity.class);
                    intent.putExtra(CourseDetailActivity.EXTRA_COURSE, c);
                    v.getContext().startActivity(intent);
                }
            });
        }
        else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    public void addCourse(Course course) {
        mCourses.add(course);
        notifyItemInserted(getItemCount() - 1);
    }

    public void addCourses(List<Course> courses) {
        mCourses.addAll(courses);
        notifyDataSetChanged();
        setLoaded();
    }

    public void popCourse() {
        int mCoursesSize = getItemCount();
        if (mCoursesSize > 0) {
            mCourses.remove(mCoursesSize - 1);
            notifyItemRemoved(getItemCount());
        }
    }

    @Override
    public int getItemCount() {
        return mCourses == null ? 0 : mCourses.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    private class CourseViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout rlCard;
        public TextView tvCourseId;
        public TextView tvTitle;
        public TextView tvPlace;

        public CourseViewHolder(View itemView) {
            super(itemView);
            rlCard = (RelativeLayout) itemView.findViewById(R.id.rlCard);
            tvCourseId = (TextView) itemView.findViewById(R.id.tvCourseId);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvPlace = (TextView) itemView.findViewById(R.id.tvPlace);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}
