package com.szoldapps.wifikursdaten.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.format.DateFormat;


public class DateHelper {

	public static enum DateHelperFormat {

		/** "hh:mm" ... e.g. "09:00" **/
		TIME,
		/** "EEEE" ... e.g. "Monday" **/
		WEEKDAY,
		/** "EEE" ... e.g. "Monday" **/
		WEEKDAY_SHORT,
		/** "YYYY/MM/DD hh:mm" ... e.g. "2012/07/16 03:00" **/
		DATE_AND_TIME,
		/** "YYYY/MM/DD hh:mm:ss.mmm" ... e.g. "2012/07/16 03:00:00.000" **/
		DATE_AND_TIME2,
		/** "EEE. MMM DD, YYYY <at> hh:mm" ... e.g. "Mon. Jul 16, 2012 at 03:00" **/
		MEDIUM_DATE_AND_TIME_WITH_WEEKDAY,
		/** "MMM DD, YYYY <at> hh:mm" ... e.g. "Jul 16, 2012 at 03:00" **/
		MEDIUM_DATE_AND_TIME,
		/** "MMM DD, YYYY" ... e.g. "Jul 16, 2012" **/
		MEDIUM_DATE_FORMAT,
		/** "YYYY-MM-DDTTT:" ... e.g. "2012-09-30T21:00:00-05:00" **/
		DATE_FOR_TRAKT,
		/** "MMM DD, YYYY" ... e.g. "Jul 16, 2012" **/
		DATE_ONLY,
		/** "MM/DD" ... e.g. "01/15" **/
		DAY_MONTH_ONLY

	}

	@SuppressLint("SimpleDateFormat")
	public static String getDateStrFromStr(String dateStr, String format, DateHelperFormat dhf, Context context) {
		String ret = "";
		if(dateStr == null) {
			return ret;
		}
		Date date = getDateFromStr(dateStr, format);
		ret = getDateStr(date, dhf, context);
		return ret;
	}

	public static Date getDateFromStr(String dateStr, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			Date date = sdf.parse(dateStr);
			return date;
		} catch (ParseException e) {
			return new Date();
		}
	}

	public static String getDateStr(Date date, DateHelperFormat dhf, Context context) {
		String ret = "";

//		java.text.DateFormat lf = DateFormat.getLongDateFormat(context);
		java.text.DateFormat mf = DateFormat.getMediumDateFormat(context);
		java.text.DateFormat df = DateFormat.getDateFormat(context);
		java.text.DateFormat tf = DateFormat.getTimeFormat(context);

		switch (dhf) {
			case TIME:
				ret = tf.format(date).toString();
				break;
			case WEEKDAY:
				ret = android.text.format.DateFormat.format("EEEE", date).toString();
				break;
			case WEEKDAY_SHORT:
				ret = android.text.format.DateFormat.format("EEE", date).toString();
				break;
			case DATE_AND_TIME:
				ret = df.format(date) + " " + tf.format(date);
				break;
			case DATE_AND_TIME2:
				ret = df.format(date) + " " + tf.format(date)+":"+android.text.format.DateFormat.format("ss", date);
				break;
			case MEDIUM_DATE_AND_TIME_WITH_WEEKDAY:
				ret = android.text.format.DateFormat.format("EEE", date) + " " + mf.format(date) + " um " + tf.format
						(date);
				break;
			case MEDIUM_DATE_AND_TIME:
				ret = mf.format(date) + " um " + tf.format(date);
				break;
			case MEDIUM_DATE_FORMAT:
				ret = mf.format(date);
				break;
			case DATE_FOR_TRAKT:
				ret = android.text.format.DateFormat.format("yyyy-MM-dd'T'HH:mm:ssz", date) + "";
				break;
			case DATE_ONLY:
				ret = mf.format(date).toString();
				break;
			case DAY_MONTH_ONLY:
				ret = android.text.format.DateFormat.format("dd.MM.", date).toString();
				break;
			default:
				break;
		}

		return ret;
	}


}
