package com.szoldapps.wifikursdaten.helper;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.szoldapps.wifikursdaten.R;
import com.szoldapps.wifikursdaten.exception.EndOfCounterException;
import com.szoldapps.wifikursdaten.model.Course;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JSONHelper {

    private final Context mContext;

    public JSONHelper(Context context) {
        mContext = context;
    }

    public List<Course> getCoursesFromJson(int start, int count) {
        JsonReader reader = new JsonReader(
                new InputStreamReader(mContext.getResources().openRawResource(R.raw.open_json_data_no_description))
        );
        Gson gson = new Gson();
        int counter = start;
        ArrayList<Course> courses = new ArrayList<>();
        int skipCounter = 0;
        try {
            reader.beginArray();
            while (reader.hasNext()) {
                // skip already loaded
                if(skipCounter++ < start) {
                    reader.skipValue();
                    continue;
                }
                // add new
                if(counter++ < count) {
                    Course course = gson.fromJson(reader, Course.class);
//                    course.setCourseId(counter);
                    courses.add(course);
                } else {
                    throw new EndOfCounterException();
                }
            }
            reader.endArray();
            reader.close();
        } catch (EndOfCounterException e) {
            try {
                reader.close();
                return courses;
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return courses;
    }
}
