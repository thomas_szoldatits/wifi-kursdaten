package com.szoldapps.wifikursdaten.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by thomasszoldatits on 20/07/16.
 */
@ToString
public class Course implements Serializable {
    @Getter @Setter
    @SerializedName("CourseId")
    private int courseId;

    @Getter @Setter
    @SerializedName("Titel")
    private String title;

    @Getter @Setter
    @SerializedName("Province")
    private String province;

    @Getter @Setter
    @SerializedName("Description")
    private String description;

    @Getter @Setter
    @SerializedName("Url")
    private String url;

    @Getter @Setter
    @SerializedName("CourseDomain")
    private String courseDomain;

    @Getter @Setter
    @SerializedName("Place")
    private String place;

    @Getter @Setter
    @SerializedName("CourseDates")
    private ArrayList<CourseDate> courseDates;

    public Course(int courseId, String title, String province) {
        this.courseId = courseId;
        this.title = title;
        this.province = province;
    }

}
