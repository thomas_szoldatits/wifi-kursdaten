package com.szoldapps.wifikursdaten.model;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.szoldapps.wifikursdaten.helper.DateHelper;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class CourseDate implements Serializable, Comparable {

    @Getter @Setter
    @SerializedName("CourseDateId")
    private int courseDateId;

    @Getter @Setter
    @SerializedName("StartDate")
    private String startDate;

    @Getter @Setter
    @SerializedName("CourseType")
    private String courseType;

    @Getter @Setter
    @SerializedName("EndDate")
    private String endDate;

    @Getter @Setter
    @SerializedName("StartTime")
    private String startTime;

    @Getter @Setter
    @SerializedName("EndTime")
    private String endTime;

    @Getter @Setter
    @SerializedName("Zip")
    private String zip;

    @Getter @Setter
    @SerializedName("Street")
    private String street;

    @Getter @Setter
    @SerializedName("Organiser")
    private String organiser;

    @Getter @Setter
    @SerializedName("Duration")
    private double duration;

    @Getter @Setter
    @SerializedName("Url")
    private String url;

    public String getStartDateString(Context context) {
        return getDateString(startDate, startTime, context);
    }

    public String getEndDateString(Context context) {
        return getDateString(endDate, endTime, context);
    }

    private String getDateString(String date, String time, Context context) {
        return DateHelper.getDateStrFromStr(date.replace("00:00:00", time), "yyyy-MM-dd'T'HH:mm:ss",
                DateHelper.DateHelperFormat.MEDIUM_DATE_AND_TIME_WITH_WEEKDAY, context);
    }

    public String getCombinedStartEndDateString(Context context) {
        return getStartDateString(context).replace("um", "von") + " bis " +
                DateHelper.getDateStrFromStr(endTime, "HH:mm:ss", DateHelper.DateHelperFormat.TIME, context);
    }

    @Override
    public int compareTo(Object another) {
        if (another instanceof CourseDate) {
            CourseDate anotherCourseDate = (CourseDate) another;
            Date thisDate = DateHelper.getDateFromStr(startDate, "yyyy-MM-dd'T'HH:mm:ss");
            Date anotherDate = DateHelper.getDateFromStr(anotherCourseDate.getStartDate(), "yyyy-MM-dd'T'HH:mm:ss");
            return (int) (anotherDate.getTime()-thisDate.getTime());
        }
        return 0;
    }
}
