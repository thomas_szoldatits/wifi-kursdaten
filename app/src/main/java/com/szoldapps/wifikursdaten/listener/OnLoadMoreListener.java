package com.szoldapps.wifikursdaten.listener;

public interface OnLoadMoreListener {
    public void onLoadMore();
}
